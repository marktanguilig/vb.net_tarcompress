﻿Imports ICSharpCode.SharpZipLib.Tar
Imports System.IO
Imports ICSharpCode.SharpZipLib.GZip

Public Class Form1

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '
        Dim openfolderdialog As New FolderBrowserDialog
        Dim savedialog As New SaveFileDialog

        openfolderdialog.ShowDialog()
        savedialog.ShowDialog()
        Dim folderpath As String = openfolderdialog.SelectedPath
        Dim savepath As String = savedialog.FileName
        CreateTarGZ(savepath & ".tar", folderpath)

    End Sub
    Private Sub CreateTarGZ(ByVal tgzFilename As String, ByVal sourceDirectory As String)
        Dim outStream As Stream = File.Create(tgzFilename)
        Dim gzoStream As Stream = New GZipOutputStream(outStream)
        Dim tarArchive__1 As TarArchive = TarArchive.CreateOutputTarArchive(gzoStream)

        tarArchive__1.RootPath = sourceDirectory.Replace("\"c, "/"c)
        If tarArchive__1.RootPath.EndsWith("/") Then
            tarArchive__1.RootPath = tarArchive__1.RootPath.Remove(tarArchive__1.RootPath.Length - 1)
        End If

        AddDirectoryFilesToTar(tarArchive__1, sourceDirectory, True)

        tarArchive__1.Close()
        End
    End Sub
    Private Sub AddDirectoryFilesToTar(ByVal tarArchive As TarArchive, ByVal sourceDirectory As String, ByVal recurse As Boolean)

        Dim tarEntry__1 As TarEntry = TarEntry.CreateEntryFromFile(sourceDirectory)
        tarArchive.WriteEntry(tarEntry__1, False)
        Dim filenames As String() = Directory.GetFiles(sourceDirectory)
        For Each filename As String In filenames
            tarEntry__1 = TarEntry.CreateEntryFromFile(filename)

            tarArchive.WriteEntry(tarEntry__1, True)
        Next

        If recurse Then
            Dim directories As String() = Directory.GetDirectories(sourceDirectory)
            For Each directory__2 As String In directories
                AddDirectoryFilesToTar(tarArchive, directory__2, recurse)
            Next
        End If
    End Sub
End Class
